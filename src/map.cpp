#include <cstring>
#include <cstdlib>
//
#include <lux_shared/common.hpp>
#include <lux_shared/map.hpp>
//
#include <physics.hpp>
#include <db.hpp>
#include <entity.hpp>
#include <worker.hpp>
#include <server.hpp>
#include "map.hpp"

namespace map {

struct SuspendedMeshUpdate {
    struct FaceUpdate {
        ChkIdx idx;
        U8     orientation;
    };
    DynArr<FaceUpdate> updated_faces;
};

struct SuspendedChunkUpdate {
    struct BlockUpdate {
        ChkIdx idx;
        Block  block;
    };
    DynArr<BlockUpdate> updated_blocks;
};

struct ChunkPhysicsMesh {
    Uninit<btTriangleIndexVertexArray> trigs_array;
    Uninit<btBvhTriangleMeshShape>     shape;

    //@NOTE for some reason we need to store these or bullet will crash the
    //      program
    DynArr<Vec3F> verts;
    DynArr<U32>   idxs;

    OwningPtr<btRigidBody> body;

    ~ChunkPhysicsMesh() {
        physics::remove_body(move(body));
        shape.deinit();
        trigs_array.deinit();
    }
};

void Chunk::Data::write(ChkIdx idx, Block const& val) {
    mutex.lock();
    has_been_changed = true;
    blocks[idx] = val;
    mutex.unlock();
}

Block Chunk::Data::read(ChkIdx idx) {
    mutex.lock_shared();
    Block rval = blocks[idx];
    mutex.unlock_shared();
    return rval;
}

void Chunk::write(ChkIdx idx, Block const& val) {
    data->write(idx, val);
}

Block Chunk::read(ChkIdx idx) {
    return data->read(idx);
}

using Mesh = ChunkMesh;

struct {
    VecMap<ChkPos, U16> meshes;
    VecMap<ChkPos, U16> chunks;
} static anchors;

constexpr Uns chunk_unload_timeout = 10; //in seconds
static VecMap<ChkPos, Uns> chunk_unload_timers;

struct {
    VecMap<HeightMapPos, worker::TaskId> generate_height_map;
    Deque<worker::TaskId>                unload_chunk;
    VecMap<ChkPos, worker::TaskId>       load_chunk;
    VecMap<ChkPos, worker::TaskId>       build_mesh;
} static enqueued_tasks;

static VecMap<ChkPos, Mesh>  meshes;
static VecMap<ChkPos, Chunk> chunks;
static VecMap<HeightMapPos, HeightMap> h_maps;

static constexpr SizeT max_suspended_chunks_updates = 256;
struct {
    VecMap<ChkPos, SuspendedMeshUpdate>  meshes;
    VecMap<ChkPos, SuspendedChunkUpdate> chunks;
} static suspended_updates;

struct {
    VecSet<ChkPos> chunks;
    VecSet<ChkPos> meshes;
} static updated_sets;

static void prepare_mesher_data(
    worker::TaskInput::BuildChunkMesh& out, ChkPos const& pos);
static bool is_chunk_loaded(ChkPos const& pos) {
    return chunks.count(pos) > 0;
}
static bool is_mesh_loaded(ChkPos const& pos) {
    return meshes.count(pos) > 0;
}
static Mesh& create_mesh(ChkPos const& pos,
                         worker::TaskOutput::BuildChunkMesh const& bcm) {
    LUX_ASSERT(not is_mesh_loaded(pos));
    Mesh& mesh = meshes[pos];
    if(bcm.faces.len == 0) {
        mesh.state = ChunkMesh::BUILT_EMPTY;
    } else {
        mesh.faces = move(bcm.faces);
        mesh.state = ChunkMesh::BUILT_TRIANGLE;
    }
    return mesh;
}
static Chunk& create_chunk(ChkPos const& pos,
                           worker::TaskOutput::LoadChunk& cl) {
    LUX_ASSERT(not is_chunk_loaded(pos));
    auto& chunk = chunks[pos];
    chunk.data = cl.data;
    cl.data = nullptr;
    for(auto const& chk : cl.block_changes) {
        LUX_ASSERT(chk.first != pos);
        if(is_chunk_loaded(chk.first)) {
            auto& chunk = chunks.at(chk.first);
            updated_sets.chunks.insert(chk.first);
            for(auto const& change : chk.second) {
                chunk.write(change.idx, change.block);
                chunk.updated_blocks.insert(change.idx);
            }
        } else {
            for(auto const& change : chk.second) {
                suspended_updates.chunks[chk.first].updated_blocks.push({
                    change.idx, change.block});
            }
        }
    }
    if(suspended_updates.chunks.count(pos) > 0) {
        for(auto const& ch : suspended_updates.chunks.at(pos).updated_blocks) {
            chunk.write(ch.idx, ch.block);
        }
        suspended_updates.chunks.erase(pos);
    }
    return chunk;
}
Mesh const& get_chunk_mesh(ChkPos const& pos) {
    return meshes.at(pos);
}
static void try_unload_chunk(ChkPos const& pos) {
    if(anchors.chunks.count(pos) == 0) {
        if(chunk_unload_timers.count(pos) == 0) {
            chunk_unload_timers[pos] = chunk_unload_timeout;
        }
    } else if(anchors.chunks.at(pos) == 0) {
        if(chunk_unload_timers.count(pos) == 0) {
            chunk_unload_timers[pos] = chunk_unload_timeout;
        }
        anchors.chunks.erase(pos);
    }
}

static worker::TaskId enqueue_chunk(ChkPos const& pos) {
    worker::TaskInput::LoadChunk in;
    in.pos = pos;
    return worker::enqueue_task(move(in));
}

void deinit() {
    //@TODO for some reason only one core works in this function???
    for(auto const& pair : enqueued_tasks.load_chunk) {
        worker::TaskOutput out(worker::TaskOutput::LOAD_CHUNK);
        worker::force_take_result(out, pair.second);
        create_chunk(pair.first, *out.load_chunk);
    }
    //we want those to finish, as they are using our chunks' data (datum?)
    //@TODO we could instead finish only the tasks that have began, and then
    //disacrd the enqueued ones
    for(auto const& pair : enqueued_tasks.build_mesh) {
        worker::TaskOutput out(worker::TaskOutput::BUILD_CHUNK_MESH);
        worker::force_take_result(out, pair.second);
        create_mesh(pair.first, *out.build_chunk_mesh);
    }
    for(auto const& pair : chunks) {
        if(pair.second.data->has_been_changed) {
            worker::TaskInput::UnloadChunk in;
            in.pos = pair.first;
            in.data = pair.second.data;
            enqueued_tasks.unload_chunk.push_back(
                worker::enqueue_task(move(in)));
        } else {
            delete pair.second.data;
        }
    }
    for(auto const& task : enqueued_tasks.unload_chunk) {
        worker::TaskOutput out(worker::TaskOutput::DISCARD);
        worker::force_take_result(out, task);
    }
    //@TODO we need to do it, because otherwise physics subsystem terminates
    //before we clean the meshes, possibly resulting in a memory leak
    meshes.clear();
}

//this function is for writing a block without really caring whether its chunk
//is loaded, if it's not, the change will be suspended and applied on load,
//still, try to write changes on chunk basis if you can, i.e. do them manually
//if you have many blocks to write, this function has some overhead
static void write_suspended_block(MapPos const& pos, Block block) {
    ChkPos chk_pos = to_chk_pos(pos);
    ChkIdx chk_idx = to_chk_idx(pos);
    if(is_chunk_loaded(chk_pos)) {
        updated_sets.chunks.insert(chk_pos);
        Chunk& chk = chunks.at(chk_pos);
        chk.write(chk_idx, block);
        chk.updated_blocks.insert(chk_idx);
    } else {
        suspended_updates.chunks[chk_pos].updated_blocks.push({chk_idx, block});
    }
}

Mesh::~Mesh() {
    if(state == BUILT_PHYSICS) {
        delete physics_mesh;
    }
}

void enqueue_missing_chunks_meshes(VecSet<ChkPos> const& requests) {
    /*static DynArr<ChkPos> loader_requests;
    loader_requests.clear();
    loader_requests.reserve_at_least(requests.size());

    for(auto const& request : requests) {
        if(mesher_requested_chunks.count(pos) == 0) {
            loader_requests.push(pos);
        }
    }*/
    LUX_UNIMPLEMENTED();
}

bool try_guarantee_chunk_mesh(ChkPos const& pos) {
    if(enqueued_tasks.build_mesh.count(pos) > 0) {
        return false;
    }
    Arr<ChkPos, 4> positions;
    Slice<ChkPos> slice = {positions, 0};

    if(is_mesh_loaded(pos)) return true;

    for(ChkPos const& off : axis_units_zero<ChkCoord>) {
        if(not is_chunk_loaded(pos + off)) {
            slice[slice.len++] = pos + off;
        }
    }
    if(slice.len > 0) {
        for(Uns i = 0; i < slice.len; ++i) {
            if(enqueued_tasks.load_chunk.count(slice[i]) == 0) {
                enqueued_tasks.load_chunk[slice[i]] = enqueue_chunk(slice[i]);
            }
        }
        return false;
    }

    //@TODO we need to anchor them before the return false, it's going to be
    //optimized, but we need to somehow make sure we don't anchor the same chunk
    //more than once
    for(auto const& off : axis_units_zero<ChkCoord>) {
        anchor_chunk(pos + off);
    }
    worker::TaskInput::BuildChunkMesh in;
    prepare_mesher_data(in, pos);
    enqueued_tasks.build_mesh[pos] = worker::enqueue_task(move(in));
    return false;
}

static void chunk_physics_mesh_rebuild(ChkPos const& pos) {
    LUX_ASSERT(is_mesh_loaded(pos));
    auto& mesh = meshes.at(pos);
    auto& p_mesh = *mesh.physics_mesh;
    for(auto const& removed_face : mesh.removed_faces) {
        for(Uns i = (removed_face + 1) * 6; i < p_mesh.idxs.len; ++i) {
            p_mesh.idxs[i] -= 4;
        }
        p_mesh.idxs.erase(removed_face * 6, 6);
        p_mesh.verts.erase(removed_face * 4, 4);
    }
    Arr<Vec3F, 3 * 4> vert_offs = {
        {0, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 1, 1},
        {0, 0, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 1},
        {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
    };
    Uns off = p_mesh.verts.len / 4;
    p_mesh.idxs.resize(p_mesh.idxs.len + mesh.added_faces.len * 6);
    p_mesh.verts.resize(p_mesh.verts.len + mesh.added_faces.len * 4);
    for(Uns i = 0; i < mesh.added_faces.len; ++i) {
        auto const& face = mesh.added_faces[i];
        ChkIdx idx = face.idx;
        U8 axis = (face.orientation & 0b110) >> 1;
        LUX_ASSERT(axis != 0b11);
        U8 sign = (face.orientation & 1);
        Vec3F face_off(0);
        face_off[axis] = 1;
        if(sign) {
            for(Uns j = 0; j < 6; ++j) {
                p_mesh.idxs[off * 6 + j] = quad_idxs<U16>[j] + off * 4;
            }
        } else {
            for(Uns j = 0; j < 6; ++j) {
                p_mesh.idxs[off * 6 + j] = quad_idxs<U16>[5 - j] + off * 4;
            }
        }
        for(Uns j = 0; j < 4; ++j) {
            auto& vert = p_mesh.verts[off * 4 + j];
            vert  = (Vec3F)to_idx_pos(idx) + face_off +
                vert_offs[axis * 4 + j];
        }
        ++off;
    }
    //@TODO better method instead of reinitializing everthing?
    p_mesh.trigs_array.deinit();
    p_mesh.trigs_array.init(
        p_mesh.idxs.len / 3, (I32*)p_mesh.idxs.beg , sizeof(I32) * 3,
        p_mesh.verts.len, (F32*)p_mesh.verts.beg, sizeof(Vec3F));

    p_mesh.shape.deinit();
    p_mesh.shape.init(
        &*p_mesh.trigs_array, true, btVector3{0, 0, 0},
        btVector3{CHK_SIZE, CHK_SIZE, CHK_SIZE});

    physics::remove_body(p_mesh.body);
    p_mesh.body = physics::create_mesh(to_map_pos(pos, 0), &*p_mesh.shape);
}

static void chunk_physics_mesh_build(ChkPos const& pos) {
    LUX_ASSERT(is_mesh_loaded(pos));
    auto& mesh = meshes.at(pos);
    if(mesh.state == Mesh::BUILT_TRIANGLE) {
        mesh.physics_mesh = new ChunkPhysicsMesh();
        auto& p_mesh = *mesh.physics_mesh;
        Uns faces_num = mesh.faces.len;
        LUX_ASSERT(faces_num > 0);
        p_mesh.verts.resize(faces_num * 4);
        p_mesh.idxs.resize(faces_num * 6);
        Arr<Vec3F, 3 * 4> vert_offs = {
            {0, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 1, 1},
            {0, 0, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 1},
            {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
        };
        for(Uns i = 0; i < faces_num; ++i) {
            auto const& face = mesh.faces[i];
            ChkIdx idx = face.idx;
            U8 axis = (face.orientation & 0b110) >> 1;
            LUX_ASSERT(axis != 0b11);
            U8 sign = (face.orientation & 1);
            Vec3F face_off(0);
            face_off[axis] = 1;
            if(sign) {
                for(Uns j = 0; j < 6; ++j) {
                    p_mesh.idxs[i * 6 + j] = quad_idxs<U16>[j] + i * 4;
                }
            } else {
                for(Uns j = 0; j < 6; ++j) {
                    p_mesh.idxs[i * 6 + j] = quad_idxs<U16>[5 - j] + i * 4;
                }
            }
            for(Uns j = 0; j < 4; ++j) {
                auto& vert = p_mesh.verts[i * 4 + j];
                vert = (Vec3F)to_idx_pos(idx) + face_off +
                    vert_offs[axis * 4 + j];
            }
        }
        p_mesh.trigs_array.init(
            faces_num * 2, (I32*)p_mesh.idxs.beg , sizeof(I32) * 3,
            p_mesh.verts.len, (F32*)p_mesh.verts.beg, sizeof(Vec3F));

        p_mesh.shape.init(
            &*p_mesh.trigs_array, true, btVector3{0, 0, 0},
            btVector3{CHK_SIZE, CHK_SIZE, CHK_SIZE});

        p_mesh.body = physics::create_mesh(to_map_pos(pos, 0), &*p_mesh.shape);
        mesh.state = Mesh::BUILT_PHYSICS;
    }
}

void guarantee_chunk(ChkPos const& pos) {
    if(is_chunk_loaded(pos)) return;
    worker::TaskId id;
    if(enqueued_tasks.load_chunk.count(pos) > 0) {
        id = enqueued_tasks.load_chunk.at(pos);
    } else {
        id = enqueue_chunk(pos);
    }
    worker::TaskOutput out(worker::TaskOutput::LOAD_CHUNK);
    worker::force_take_result(out, id);
    create_chunk(pos, *out.load_chunk);
    enqueued_tasks.load_chunk.erase(pos);
}

void guarantee_physics_mesh_for_aabb(MapPos const& min, MapPos const& max) {
    //@TODO we need to unload the chunks and meshes here (who owns the mesh
    //anchor?)
    ChkPos const c_min = to_chk_pos(min);
    ChkPos const c_max = to_chk_pos(max);
    LUX_ASSERT(glm::all(glm::lessThanEqual(c_min, c_max)));
    ChkPos iter;

    //first we do a quick discard of already built meshes (most of the cases)
    bool needs_generation = false;
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
            for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
                if(not is_mesh_loaded(iter) ||
                  (meshes.at(iter).state != Mesh::BUILT_PHYSICS &&
                   meshes.at(iter).state != Mesh::BUILT_EMPTY)) {
                    needs_generation = true;
                }
            }
        }
    }
    if(not needs_generation) return;

    //@TODO we try to load chunks even though the meshes might have been built
    static DynArr<ChkPos> loader_requests;
    loader_requests.clear();
    ChkPos const c_size = (c_max + (ChkCoord)1) - c_min;
    loader_requests.reserve_at_least(glm::compMul(c_size) +
        c_size.x * c_size.y + c_size.x * c_size.z + c_size.y * c_size.z);
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
            for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
                if(!is_chunk_loaded(iter)) {
                    loader_requests.emplace(iter);
                }
            }
        }
    }
    iter.x = c_max.x + (ChkCoord)1;
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
            if(!is_chunk_loaded(iter)) {
                loader_requests.emplace(iter);
            }
        }
    }
    iter.y = c_max.y + (ChkCoord)1;
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
            if(!is_chunk_loaded(iter)) {
                loader_requests.emplace(iter);
            }
        }
    }
    iter.z = c_max.z + (ChkCoord)1;
    for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
        for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
            if(!is_chunk_loaded(iter)) {
                loader_requests.emplace(iter);
            }
        }
    }
    if(loader_requests.len > 0) {
        for(auto const& request : loader_requests) {
            if(enqueued_tasks.load_chunk.count(request) == 0) {
                enqueued_tasks.load_chunk[request] = enqueue_chunk(request);
            }
        }
        for(auto const& request : loader_requests) {
            worker::TaskOutput out(worker::TaskOutput::LOAD_CHUNK);
            worker::force_take_result(out,
                enqueued_tasks.load_chunk.at(request));
            create_chunk(request, *out.load_chunk);
            enqueued_tasks.load_chunk.erase(request);
        }
    }

    //now we build the meshes
    static DynArr<worker::TaskInput::BuildChunkMesh> mesher_requests;
    static DynArr<ChkPos> needed_meshes;
    mesher_requests.clear();
    needed_meshes.clear();
    mesher_requests.reserve_at_least(glm::compMul(c_size));
    needed_meshes.reserve_at_least(glm::compMul(c_size));
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
            for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
                if(not is_mesh_loaded(iter)) {
                    if(enqueued_tasks.build_mesh.count(iter) > 0) {
                        needed_meshes.emplace(iter);
                        continue;
                    }
                    mesher_requests.emplace();
                    auto& in = mesher_requests.last();
                    prepare_mesher_data(in, iter);
                    needed_meshes.emplace(iter);
                }
            }
        }
    }
    for(auto&& request : mesher_requests) {
        enqueued_tasks.build_mesh[request.pos] =
            worker::enqueue_task(move(request));
    }
    for(auto const& mesh_pos : needed_meshes) {
        worker::TaskOutput out(worker::TaskOutput::BUILD_CHUNK_MESH);
        //@TODO this is pretty inefficient, I think the main thread will
        //just wait here instead of helping doing the tasks
        worker::force_take_result(out,
            enqueued_tasks.build_mesh.at(mesh_pos));
        auto& mesh = create_mesh(mesh_pos, *out.build_chunk_mesh);
        enqueued_tasks.build_mesh.erase(mesh_pos);
    }
    //@TODO also try unload chunks that we load here

    //finally we build the physics meshes
    for(iter.z = c_min.z; iter.z <= c_max.z; ++iter.z) {
        for(iter.y = c_min.y; iter.y <= c_max.y; ++iter.y) {
            for(iter.x = c_min.x; iter.x <= c_max.x; ++iter.x) {
                chunk_physics_mesh_build(iter);
            }
        }
    }
}

void write_block(MapPos const& pos, Block const& val) {
    ChkPos chk_pos = to_chk_pos(pos);
    LUX_ASSERT(is_chunk_loaded(chk_pos));
    Chunk& chunk = chunks.at(chk_pos);
    ChkIdx chk_idx = to_chk_idx(pos);
    //@TODO compare blocks directly here
    if(chunk.read(chk_idx).id == val.id) {
        return;
    }
    chunk.write(chk_idx, val);
    updated_sets.chunks.emplace(chk_pos);
    chunk.updated_blocks.insert(chk_idx);
}

Block read_block(MapPos const& pos) {
    LUX_ASSERT(is_chunk_loaded(to_chk_pos(pos)));
    return chunks.at(to_chk_pos(pos)).read(to_chk_idx(pos));
}

BlockBp const& get_block_bp(MapPos const& pos) {
    return db_block_bp(read_block(pos).id);
}

static void add_face(Uns axis, MapPos map_pos) {
    //@NOTE we assume the mesh will update with ANY block
    //updates, which is true, unless we add something like
    //a different kind of air, then a block update might not
    //change the mesh
    MapPos other_pos = map_pos + axis_units<MapCoord>[axis];
    ChkPos mesh_pos = to_chk_pos(map_pos);
    ChkIdx face_idx = to_chk_idx(map_pos);
    if(is_mesh_loaded(mesh_pos)) {
        updated_sets.meshes.insert(mesh_pos);
        Mesh& mesh = meshes.at(mesh_pos);
        switch(mesh.state) {
            //we assume previously empty meshes have been turned
            //into triangle meshes
            case Mesh::BUILT_EMPTY : LUX_UNREACHABLE(); break;
            case Mesh::BUILT_TRIANGLE :
            case Mesh::BUILT_PHYSICS: {
                guarantee_chunk(mesh_pos);
                guarantee_chunk(to_chk_pos(other_pos));
                auto const& b0 = read_block(map_pos);
                auto const& b1 = read_block(other_pos);
                if((b0.id == void_block) !=
                   (b1.id == void_block)) {
                    bool orient = b0.id == void_block;
                    BlockId id = not orient ? b0.id : b1.id;
                    mesh.faces.push({face_idx, id,
                        (((U8)axis << 1) | (U8)(not orient))});
                    mesh.added_faces.emplace(mesh.faces.last());
                }
                break;
            }
            default: LUX_UNREACHABLE();
        }
    } else if(enqueued_tasks.build_mesh.count(mesh_pos) > 0) {
        //@TODO change the BlockFace to something with no
        //orientation and no ID
        suspended_updates.meshes[mesh_pos].updated_faces.push({face_idx,
            (axis << 1)});
    }
    //if we haven't even requested a mesh, then it's simply
    //going to build with the new voxel data, i.e. there is
    //nothing to update
};

static void remove_face(Uns axis, MapPos map_pos) {
    //@NOTE we assume the mesh will update with ANY block
    //updates, which is true, unless we add something like
    //a different kind of air, then a block update might not
    //change the mesh
    ChkPos mesh_pos = to_chk_pos(map_pos);
    ChkIdx face_idx = to_chk_idx(map_pos);
    if(is_mesh_loaded(mesh_pos)) {
        updated_sets.meshes.insert(mesh_pos);
        Mesh& mesh = meshes.at(mesh_pos);
        switch(mesh.state) {
            case Mesh::BUILT_EMPTY : {
                mesh.state = Mesh::BUILT_TRIANGLE;
                break;
            }
            case Mesh::BUILT_TRIANGLE :
            case Mesh::BUILT_PHYSICS: {
                for(Uns i = 0; i < mesh.faces.len; ++i) {
                    auto const& face = mesh.faces[i];
                    if(face.idx == face_idx &&
                       (face.orientation & 0b110) >> 1 == axis) {
                        mesh.faces.erase(i);
                        mesh.removed_faces.emplace(i);
                        break;
                    }
                }
                break;
            }
            default: LUX_UNREACHABLE();
        }
    }
    //if we haven't even requested a mesh, then it's simply
    //going to build with the new voxel data, i.e. there is
    //nothing to update
};

void tick(TickTimeData const& time_data) {
    benchmark("tick", 1.0 / time_data.tick_rate, [&](){
    //our meshes might be representing old voxel data, that's why we need to
    //load respective chunks and apply the updates, which will in turn trigger
    //mesh updates
    for(auto const& pair : suspended_updates.chunks) {
        auto const& pos = pair.first;
        if(enqueued_tasks.load_chunk.count(pos) > 0) {
            continue;
        }
        bool should_load = false;
        for(auto const& off : axis_units_zero<ChkCoord>) {
            if(is_mesh_loaded(pos + off)) {
                should_load = true;
                break;
            }
        }
        if(should_load) {
            enqueued_tasks.load_chunk[pos] = enqueue_chunk(pos);
        }
    }

    //receive chunks loaded from worker threads
    for(auto it = enqueued_tasks.load_chunk.begin(),
            end = enqueued_tasks.load_chunk.end();
            it != end;) {
        worker::TaskOutput out(worker::TaskOutput::LOAD_CHUNK);
        if(worker::try_take_result(out, it->second)) {
            create_chunk(it->first, *out.load_chunk);
            it = enqueued_tasks.load_chunk.erase(it);
        } else it++;
    }
    //receive meshes loaded from worker threads
    for(auto it = enqueued_tasks.build_mesh.begin(),
            end = enqueued_tasks.build_mesh.end();
            it != end;) {
        worker::TaskOutput out(worker::TaskOutput::BUILD_CHUNK_MESH);
        if(worker::try_take_result(out, it->second)) {
            ChkPos const& pos = it->first;
            auto& mesh = create_mesh(pos, *out.build_chunk_mesh);
            if(suspended_updates.meshes.count(pos) > 0) {
                //we always remove first, and so we need two iterations over
                //the updates
                for(auto const& face :
                    suspended_updates.meshes.at(pos).updated_faces) {
                    MapPos map_pos = to_map_pos(pos, face.idx);
                    remove_face(face.orientation >> 1, map_pos);
                }
                for(auto const& face :
                    suspended_updates.meshes.at(pos).updated_faces) {
                    MapPos map_pos = to_map_pos(pos, face.idx);
                    add_face(face.orientation >> 1, map_pos);
                }
                if(mesh.faces.len == 0 && mesh.state != Mesh::BUILT_EMPTY) {
                    mesh.state = Mesh::BUILT_EMPTY;
                }
                suspended_updates.meshes.erase(it->first);
            }
            for(auto const& off : axis_units_zero<ChkCoord>) {
                deanchor_chunk(pos + off);
            }
            it = enqueued_tasks.build_mesh.erase(it);
        } else it++;
    }
    //@TODO not needed
    for(auto it = enqueued_tasks.unload_chunk.begin(),
            end = enqueued_tasks.unload_chunk.end();
            it != end;) {
        worker::TaskOutput out(worker::TaskOutput::DISCARD);
        if(worker::try_take_result(out, *it)) {
            it = enqueued_tasks.unload_chunk.erase(it);
        } else it++;
    }
    });

    static auto const& snow = db_block_id("snow"_l);
    benchmark("chunk updates", 1.0 / time_data.tick_rate, [&](){
#if 0 //used to benchmark chunk updates
    if(not (tick_num % 16)) {
    for(auto& chk : chunks) {
        static Uns seed = 0;
        if(lux_randf(0, seed++) < 0.99f) continue;
        for(Uns i = 0; i < CHK_VOL; ++i) {
            if(chk.second.read(i).id == void_block && lux_randf(seed++) > 0.999f) {
                write_block(to_map_pos(chk.first, i), {lux_randmm(0, (1 << 16) - 1, seed++)});
            }
        }
    }
    }
#endif

    //@TODO move this loop
    while(updated_sets.chunks.size() > 0) {
        auto temp_updated_chunks(move(updated_sets.chunks));
        LUX_ASSERT(updated_sets.chunks.size() == 0);
        for(auto const& pos : temp_updated_chunks) {
            LUX_ASSERT(is_chunk_loaded(pos));
            while(chunks.at(pos).updated_blocks.size() > 0) {
                auto temp_updated_blocks(move(chunks.at(pos).updated_blocks));
                //we always remove first, and so we need two iterations over the updates
                for(auto const& idx : temp_updated_blocks) {
                    for(Uns a = 0; a < 3; ++a) {
                        MapPos map_pos = to_map_pos(pos, idx);
                        //we remove the face belonging to the previous voxel, i.e. the
                        //negative face
                        remove_face(a, map_pos - axis_units<MapCoord>[a]);
                        //and then the local positive face
                        remove_face(a, map_pos);
                    }
                }
                for(auto const& idx : temp_updated_blocks) {
                    for(Uns a = 0; a < 3; ++a) {
                        MapPos map_pos = to_map_pos(pos, idx);
                        //we add the face belonging to the previous voxel, i.e. the
                        //negative face
                        add_face(a, map_pos - axis_units<MapCoord>[a]);
                        //and then the local positive face
                        add_face(a, map_pos);
                    }
                }
            }
        }
        for(auto const& pos : updated_sets.meshes) {
            LUX_ASSERT(is_mesh_loaded(pos));
            Mesh& mesh = meshes.at(pos);
            if(mesh.faces.len == 0 && mesh.state != Mesh::BUILT_EMPTY) {
                if(mesh.state == Mesh::BUILT_PHYSICS) {
                    delete mesh.physics_mesh;
                }
                mesh.state = Mesh::BUILT_EMPTY;
            } else if(mesh.state == Mesh::BUILT_PHYSICS) {
                chunk_physics_mesh_rebuild(pos);
            }
        }
    }
    });
    physics::tick(time_data.delta); //TODO tick time
    if(not (time_data.tick_num % time_data.tick_rate)) {
        LUX_ASSERT(updated_sets.chunks.size() == 0);
        benchmark("chunk unloading", (1.0 / 64.0) / 4.0, [&](){
        for(auto it = chunk_unload_timers.begin(),
                end = chunk_unload_timers.end();
                 it != end;) {
            auto const& pos = it->first;
            auto&       num = it->second;
            LUX_ASSERT(is_chunk_loaded(pos));
            num--;
            if(num == 0) {
                Chunk& chunk = chunks.at(pos);
                if(chunk.data->has_been_changed) {
                    worker::TaskInput::UnloadChunk in;
                    in.pos = pos;
                    in.data = chunks.at(pos).data;
                    enqueued_tasks.unload_chunk.push_back(
                        worker::enqueue_task(move(in)));
                } else {
                    delete chunk.data;
                }
                chunks.erase(pos);
                it = chunk_unload_timers.erase(it);
            } else it++;
        }
        chunks.reserve(chunks.size());
        });
        static Uns last_mesh_sz = meshes.size();
        if(meshes.size() < last_mesh_sz) {
            meshes.reserve(meshes.size());
        }
        last_mesh_sz = meshes.size();
    }
    server::send_mesh_updates(updated_sets.meshes);
    for(auto const& pos : updated_sets.meshes) {
        LUX_ASSERT(is_mesh_loaded(pos));
        auto& mesh = meshes.at(pos);
        mesh.removed_faces.clear();
        mesh.added_faces.clear();
    }
    updated_sets.meshes.clear();
}

bool cast_ray(MapPos* out_pos, Vec3F* out_dir, Vec3F src, Vec3F dst) {
    Vec3F ray = dst - src;
    Vec3F a = abs(ray);
    int max_i;
    if(a.x > a.y) {
        if(a.x > a.z) max_i = 0;
        else          max_i = 2;
    } else {
        if(a.y > a.z) max_i = 1;
        else          max_i = 2;
    }
    F32 max = a[max_i];
    Vec3F dt = ray / max;
    Vec3F fr = src - trunc(src);
    F32    o = fr[max_i];

    Vec3F it = src + o * dt;

    for(Uns i = 0; i < max; ++i) {
        for(Uns j = 0; j < 3; ++j) {
            it[j] += dt[j];
            MapPos map_pos = floor(it);
            ChkPos chk_pos = to_chk_pos(map_pos);
            //@TODO
            anchor_chunk(chk_pos);
            guarantee_chunk(chk_pos);
            //if(!is_chunk_loaded(chk_pos)) return false;
            //@TODO cache chunks for much better speed
            if(chunks.at(chk_pos).read(to_chk_idx(map_pos)).id != void_block) {
                *out_pos = map_pos;
                Vec3F norm(0.f);
                norm[j] = ray[j] > 0.f ? 1.f : -1.f;
                *out_dir = norm;
                return true;
            }
        }
    }
    return false;
}

static void prepare_mesher_data(
    worker::TaskInput::BuildChunkMesh& out, ChkPos const& pos) {
    out.pos = pos;
    for(Uns a = 0; a < 4; ++a) {
        out.data[a] = chunks.at(pos + axis_units_zero<ChkCoord>[a]).data;
    }
}

void anchor_chunk(ChkPos const& pos) {
    if(anchors.chunks.count(pos) == 0) {
        anchors.chunks[pos] = 0;
    }
    if(chunk_unload_timers.count(pos) > 0) {
        chunk_unload_timers.erase(pos);
    }
    auto& val = anchors.chunks.at(pos);
    LUX_ASSERT(val < static_max_val(val));
    val++;
}

void deanchor_chunk(ChkPos const& pos) {
    LUX_ASSERT(anchors.chunks.count(pos) > 0);
    auto& val = anchors.chunks.at(pos);
    LUX_ASSERT(val > 0);
    val--;
    try_unload_chunk(pos);
}

void anchor_mesh(ChkPos const& pos) {
    if(anchors.meshes.count(pos) == 0) {
        anchors.meshes[pos] = 0;
    }
    auto& val = anchors.meshes.at(pos);
    LUX_ASSERT(val < static_max_val(val));
    val++;
}

void deanchor_mesh(ChkPos const& pos) {
    LUX_ASSERT(anchors.meshes.count(pos) > 0);
    auto& val = anchors.meshes.at(pos);
    LUX_ASSERT(val > 0);
    val--;
    if(val == 0) {
        meshes.erase(pos);
        anchors.meshes.erase(pos);
        updated_sets.meshes.erase(pos);
    }
}

ChunkAnchor::ChunkAnchor(ChkPos _pos) {
    is_owning = true;
    pos = _pos;
    anchor_chunk(pos);
}

ChunkAnchor::~ChunkAnchor() {
    if(is_owning) {
        deanchor_chunk(pos);
    }
}

MeshAnchor::MeshAnchor(ChkPos _pos) {
    is_owning = true;
    pos = _pos;
    anchor_mesh(pos);
}

MeshAnchor::~MeshAnchor() {
    if(is_owning) {
        deanchor_mesh(pos);
    }
}

ChunkAnchor::ChunkAnchor(ChunkAnchor&& that) {
    *this = move(that);
}

ChunkAnchor& ChunkAnchor::operator=(ChunkAnchor&& that) {
    pos = that.pos;
    is_owning = that.is_owning;
    that.is_owning = false;
    return *this;
}

MeshAnchor::MeshAnchor(MeshAnchor&& that) {
    *this = move(that);
}

MeshAnchor& MeshAnchor::operator=(MeshAnchor&& that) {
    pos = that.pos;
    is_owning = that.is_owning;
    that.is_owning = false;
    return *this;
}

}
