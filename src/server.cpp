#include <cstring>
#include <algorithm>
//
#include <enet/enet.h>
//
#include <lux_shared/common.hpp>
#include <lux_shared/net/common.hpp>
#include <lux_shared/net/data.hpp>
#include <lux_shared/net/data.inl>
#include <lux_shared/net/enet.hpp>
//
#include <map.hpp>
#include <entity.hpp>
#include "server.hpp"

namespace server {

static constexpr Uns max_clients = 16;
static U16 tick_rate = 0.0;

struct Client {
    ENetPeer*                       peer;
    Arr<char, CLIENT_NAME_LEN>      name;
    EntityId                        entity;
    bool                            is_admin = false;
    VecMap<ChkPos, map::MeshAnchor> loaded_meshes;
    VecMap<ChkPos, map::MeshAnchor> pending_requests;
};

static SparseDynArr<Client> clients;
static bool is_running = false;
static ENetHost* host;

struct {
    NetCsTick tick;
} cs; //client-side

struct {
    NetSsInit init;
    NetSsSgnl sgnl;
    NetSsTick tick;
} ss; //server-side

void init(U16 port, F64 _tick_rate) {
    tick_rate = _tick_rate;

    LUX_LOG("initializing server");
    if(enet_initialize() != 0) {
        LUX_FATAL("couldn't initialize ENet");
    }

    {
        ENetAddress addr = {ENET_HOST_ANY, port};
        host = enet_host_create(&addr, max_clients, CHANNEL_NUM, 0, 0);
        if(host == nullptr) {
            LUX_FATAL("couldn't initialize ENet host");
        }
    }
    net_compression_init(host);
    is_running = true;
}

static Client& get_client(ENetPeer* peer) {
    return clients[(ClientId)peer->data];
}

void deinit() {
    is_running = false;
    LUX_LOG("deinitializing server");

    LUX_LOG("kicking all clients");
    for(auto pair : clients) {
        kick_client(pair.k, "server stopping"_l);
    }
    enet_host_destroy(host);
    enet_deinitialize();
}

bool is_client_connected(ClientId id) {
    return clients.contains(id);
}

void erase_client(ClientId id) {
    LUX_ASSERT(is_client_connected(id));
    LUX_LOG("client disconnected");
    LUX_LOG("    id: %zu" , id);
    auto const& name = clients[id].name;
    LUX_LOG("    name: %.*s", CLIENT_NAME_LEN, name);
    entity::erase(clients[id].entity);
    clients.erase(id);
}

void kick_peer(ENetPeer *peer) {
    U8* ip = get_ip(peer->address);
    LUX_LOG("terminating connection with peer");
    LUX_LOG("    ip: %u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
    enet_peer_disconnect_now(peer, 0);
}

LUX_MAY_FAIL static send_msg(ClientId id, Str str) {
    LUX_UNIMPLEMENTED();
    return LUX_OK;
}

void kick_client(ClientId id, Str reason) {
    LUX_ASSERT(is_client_connected(id));
    DynStr name = (DynStr)clients[id].name;
    LUX_LOG("kicking client");
    LUX_LOG("    name: %.*s", CLIENT_NAME_LEN, name.beg);
    LUX_LOG("    reason: %.*s", (int)reason.len, reason.beg);
    LUX_LOG("    id: %zu", id);
    Str constexpr prefix = "you got kicked for: "_l;
    StrBuff msg(prefix.len + reason.len);
    msg.cpy(prefix).cpy(reason);
    (void)send_msg(id, msg);
    enet_host_flush(host);
    kick_peer(clients[id].peer);
    erase_client(id);
}

LUX_MAY_FAIL add_client(ENetPeer* peer) {
    U8* ip = get_ip(peer->address);
    LUX_LOG("new client connecting");
    LUX_LOG("    ip: %u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);

    ENetPacket* in_pack;

    { ///retrieve init packet
        LUX_LOG("awaiting init packet");
        Uns constexpr MAX_TRIES = 10;
        Uns constexpr TRY_TIME  = 50; ///in milliseconds

        //@CONSIDER sleeping in a separate thread, so the server cannot be
        //frozen by malicious joining, perhaps a different, sleep-free solution
        //could be used
        Uns tries = 0;
        U8  channel_id;
        do {
            enet_host_service(host, nullptr, TRY_TIME);
            in_pack = enet_peer_receive(peer, &channel_id);
            if(in_pack != nullptr) {
                if(channel_id == INIT_CHANNEL) {
                    break;
                } else {
                    LUX_LOG("ignoring unexpected packet");
                    LUX_LOG("    channel: %u", channel_id);
                    enet_packet_destroy(in_pack);
                }
            }
            if(tries >= MAX_TRIES) {
                LUX_LOG("client did not send an init packet");
                return LUX_FAIL;
            }
            ++tries;
        } while(true);
        LUX_LOG("received init packet after %zu/%zu tries", tries, MAX_TRIES);
    }

    NetCsInit cs_init;
    { ///parse client init packet
        LUX_DEFER { enet_packet_destroy(in_pack); };
        LUX_RETHROW(deserialize_packet(in_pack, &cs_init),
            "failed to deserialize init packet from client");

        if(cs_init.net_ver.major != NET_VERSION_MAJOR) {
            LUX_LOG("client uses an incompatible major lux net api version");
            LUX_LOG("    ours: %u", NET_VERSION_MAJOR);
            LUX_LOG("    theirs: %u", cs_init.net_ver.major);
            return LUX_FAIL;
        }
        if(cs_init.net_ver.minor >  NET_VERSION_MINOR) {
            LUX_LOG("client uses a newer minor lux net api version");
            LUX_LOG("    ours: %u", NET_VERSION_MINOR);
            LUX_LOG("    theirs: %u", cs_init.net_ver.minor);
            return LUX_FAIL;
        }

        ClientId duplicate_id = get_client_id((Str)cs_init.name);
        if(duplicate_id != clients.end().id) {
            //@CONSIDER kicking the new one instead
            LUX_LOG("client already connected, kicking the old one");
            kick_client(duplicate_id, "double-join"_l);
        }
    }

    ClientId id = clients.emplace();
    Client& client = clients[id];
    client.peer = peer;
    client.peer->data = (void*)id;
    memcpy(client.name, cs_init.name, sizeof(char) * CLIENT_NAME_LEN);

    { ///send init packet
        Str constexpr name = "lux-server"_l;
        LUX_ASSERT(name.len <= SERVER_NAME_LEN);
        ((DynStr)ss.init.name).cpy(name).set('\0');
        ss.init.tick_rate    = tick_rate;
        if(send_net_data(peer, &ss.init, INIT_CHANNEL) != LUX_OK) {
            LUX_LOG_ERR("failed to send init data to client");
            return LUX_FAIL;
        }
    }
    client.entity = entity::create_player();

    LUX_LOG("client connected successfully");
    LUX_LOG("    name: %.*s", CLIENT_NAME_LEN, client.name);
#ifndef NDEBUG
    (void)make_admin(id);
#endif
    return LUX_OK;
}

LUX_MAY_FAIL send_chunk_mesh_update(ENetPeer* peer,
                                    VecSet<ChkPos> const& meshes) {
    ss.sgnl.tag = NetSsSgnl::CHUNK_MESH_UPDATE;
    for(auto const& pos : meshes) {
        auto const& mesh = map::get_chunk_mesh(pos);;
        ss.sgnl.chunk_mesh_update.meshes[pos].removed_faces = mesh.removed_faces;
        ss.sgnl.chunk_mesh_update.meshes[pos].added_faces   = mesh.added_faces;
    }

    LUX_RETHROW(send_net_data(peer, &ss.sgnl, SGNL_CHANNEL),
        "failed to send chunk updates to client");
    return LUX_OK;
}

LUX_MAY_FAIL handle_tick(ENetPeer* peer, ENetPacket *in_pack) {
    LUX_ASSERT(is_client_connected((ClientId)peer->data));
    LUX_RETHROW(deserialize_packet(in_pack, &cs.tick),
        "failed to deserialize tick from client")

    auto const& client = get_client(peer);
    Uninit<entity::PlayerController> u_player;
    if(entity::try_get_controller(u_player, client.entity)) {
        auto& player = *u_player;
        player.set_yaw_pitch(cs.tick.yaw_pitch.x, cs.tick.yaw_pitch.y);
        if(cs.tick.is_moving) {
            F32 len = length(cs.tick.move_dir);
            if(len != 1.f && len != 0.f) {
                cs.tick.move_dir /= len;
            }
            player.move(cs.tick.move_dir);
        }
        if(cs.tick.is_jumping) {
            player.jump();
        }
    } else {
        //@TODO some kind of error
    }
    return LUX_OK;
}

LUX_MAY_FAIL handle_signal(ENetPeer* peer, ENetPacket* in_pack) {
    NetCsSgnl sgnl;
    LUX_RETHROW(deserialize_packet(in_pack, &sgnl),
        "failed to deserialize signal from client");

    Client& client = get_client(peer);
    switch(sgnl.tag) {
        case NetCsSgnl::CHUNK_MESH_REQUEST: {
            for(auto const& request : sgnl.chunk_mesh_request.requests) {
                client.pending_requests.emplace(request, request);
                map::try_guarantee_chunk_mesh(request);
            }
            break;
        }
        case NetCsSgnl::CHUNK_MESH_UNLOAD: {
            for(auto const& chk_pos : sgnl.chunk_mesh_unload.chunks) { //@TODO chunks->meshes
                client.pending_requests.erase(chk_pos);
                client.loaded_meshes.erase(chk_pos);
            }
            break;
        }
        case NetCsSgnl::DO_ACTION: {
            Uninit<entity::PlayerController> u_player;
            if(not entity::try_get_controller(u_player, client.entity)) {
                LUX_LOG_ERR("failed to get player controller handle");
                return LUX_FAIL;
            }
            auto& player = *u_player;
            switch(sgnl.do_action) {
                case NetCsSgnl::DoAction::BREAK_BLOCK: {
                    player.break_block();
                    break;
                }
                case NetCsSgnl::DoAction::PLACE_BLOCK: {
                    player.place_block();
                    break;
                }
                //@TODO how about a macro for default : LUX_UNREACHABLE();
                default: LUX_UNREACHABLE();
            }
            break;
        }
        case NetCsSgnl::DO_INTERACTION: {
            //@TODO merge with do action?
            Uninit<entity::PlayerController> u_player;
            if(not entity::try_get_controller(u_player, client.entity)) {
                LUX_LOG_ERR("failed to get player controller handle");
                return LUX_FAIL;
            }
            auto& player = *u_player;
            switch(sgnl.do_interaction.tag) {
                case NetCsSgnl::DoInteraction::EAT: {
                    player.eat(sgnl.do_interaction.id);
                    break;
                }
                default: LUX_UNREACHABLE();
            }
            break;
        };
        default: LUX_UNREACHABLE();
    }
    return LUX_OK;
}

//@TODO chunk -> mesh
static void handle_pending_chunk_requests(Client& client) {
    if(client.pending_requests.size() == 0) return;
    ss.sgnl.tag = NetSsSgnl::CHUNK_MESH_LOAD;

    static VecSet<ChkPos> loaded_meshes;
    loaded_meshes.clear();
    for(auto const& pair : client.pending_requests) {
        auto const& pos = pair.first;
        //@TODO we should really serialize this
        if(map::try_guarantee_chunk_mesh(pos)) {
            map::ChunkMesh const& mesh = map::get_chunk_mesh(pos);
            auto& net_mesh = ss.sgnl.chunk_mesh_load.meshes[pos];
            if(mesh.state != map::ChunkMesh::BUILT_EMPTY) {
                net_mesh.faces.resize(mesh.faces.len);
                for(Uns i = 0; i < mesh.faces.len; ++i) {
                    net_mesh.faces[i].idx         = mesh.faces[i].idx;
                    net_mesh.faces[i].id          = mesh.faces[i].id;
                    net_mesh.faces[i].orientation = mesh.faces[i].orientation;
                }
            }
            loaded_meshes.insert(pos);
        }
    }

    if(send_net_data(client.peer, &ss.sgnl, SGNL_CHANNEL) == LUX_OK) {
        //we want to be sure that the chunks have been sent,
        //so that there are no chunks that never load
        for(auto const& pos : loaded_meshes) {
            client.loaded_meshes.emplace(pos,
                move(client.pending_requests.at(pos)));
            client.pending_requests.erase(pos);
        }
    } else {
        LUX_LOG_ERR("failed to send map load data to client");
    }
}

void send_mesh_updates(VecSet<ChkPos> const& meshes) {
    benchmark("1", 1.0 / 64.0, [&](){
    for(auto pair : clients) {
        static VecSet<ChkPos> meshes_to_send;
        for(auto const& pos : meshes) {
            if(pair.v.loaded_meshes.count(pos) > 0) {
                meshes_to_send.insert(pos);
            }
        }
        if(meshes_to_send.size() > 0) {
            (void)send_chunk_mesh_update(pair.v.peer, meshes_to_send);
            meshes_to_send.clear();
        }
    }
    });
}

void tick(TickTimeData const& time_data) {
    benchmark("2", 1.0 / 64.0, [&](){
    { ///handle events
        ENetEvent event;
        while(enet_host_service(host, &event, 0) > 0) {
            if(event.type == ENET_EVENT_TYPE_CONNECT) {
                if(add_client(event.peer) != LUX_OK) {
                    kick_peer(event.peer);
                }
            } else if(event.type == ENET_EVENT_TYPE_DISCONNECT) {
                erase_client((ClientId)event.peer->data);
            } else if(event.type == ENET_EVENT_TYPE_RECEIVE) {
                LUX_DEFER { enet_packet_destroy(event.packet); };
                if(!is_client_connected((ClientId)event.peer->data)) {
                    U8 *ip = get_ip(event.peer->address);
                    LUX_LOG("ignoring packet from not connected peer");
                    LUX_LOG("    ip: %u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
                    enet_peer_reset(event.peer);
                } else {
                    if(event.channelID == TICK_CHANNEL) {
                        if(handle_tick(event.peer, event.packet) != LUX_OK) {
                            continue;
                        }
                    } else if(event.channelID == SGNL_CHANNEL) {
                        if(handle_signal(event.peer, event.packet) != LUX_OK) {
                            LUX_LOG("failed to handle signal from client");
                            kick_client((ClientId)event.peer->data,
                                        "corrupted signal packet"_l);
                            continue;
                        }
                    } else {
                        auto const &name = get_client(event.peer).name;
                        LUX_LOG("ignoring unexpected packet");
                        LUX_LOG("    channel: %u", event.channelID);
                        LUX_LOG("    from: %.*s", CLIENT_NAME_LEN, name);
                    }
                }
            }
        }
    }
    });

    benchmark("3", 1.0 / 64.0, [&](){
    { ///dispatch ticks and other pending packets
        //@IMPROVE we might want to turn this into a differential transfer,
        //instead of resending the whole state all the time
        entity::get_net_entities(ss.tick.entities);
        ss.tick.day_cycle = time_data.day_cycle;
        for(auto pair : clients) {
            auto& client = pair.v;
            ss.tick.player_id = client.entity;
            (void)send_net_data(client.peer, &ss.tick, TICK_CHANNEL, false);

            handle_pending_chunk_requests(client);
        }
        clear_net_data(&ss.tick);
    }
    clients.free_slots();
    });
}

void broadcast(Str msg) {
    for(auto pair : clients) {
        (void)send_msg(pair.k, msg);
    }
}

bool get_is_running() {
    return is_running;
}

void stop() {
    is_running = false;
}

ClientId get_client_id(Str name) {
    ClientId rval = clients.end().id;
    for(auto pair : clients) {
        auto const& client = pair.v;
        if((Str)client.name == name) {
            rval = pair.k;
            break;
        }
    }
    return rval;
}

void make_admin(ClientId id) {
    LUX_LOG("making client %zu an admin", id);
    LUX_ASSERT(is_client_connected(id));
    clients[id].is_admin = true;
}

}
