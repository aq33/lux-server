#pragma once

#include <lux_shared/common.hpp>

namespace worker {

using TaskId = U64;

struct TaskInput;
struct TaskOutput;

//@TODO just use the multi version always?
template<typename T>
TaskId enqueue_task(T&& in);

/*template<typename T>
TaskId enqueue_tasks(Slice<T> const& ins);*/

bool try_take_result(TaskOutput& out, TaskId id);
void force_take_result(TaskOutput& out, TaskId id);

Uns task_queue_len();

void init();
void deinit();

}

#include <worker_impl.hpp>
