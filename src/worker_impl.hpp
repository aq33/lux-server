#pragma once

#include <lux_shared/common.hpp>
#include <lux_shared/map.hpp>
//
#include <map.hpp>

namespace worker {

struct TaskInput {
    enum Type : U8 {
        GENERATE_HEIGHT_MAP,
        LOAD_CHUNK,
        UNLOAD_CHUNK,
        BUILD_CHUNK_MESH,
    } type;

    TaskId id;

    struct GenerateHeightMap {
        map::HeightMapPos pos;
    };
    struct LoadChunk {
        ChkPos pos;
        SharingPtr<map::HeightMap> h_maps[3];
    };
    struct BuildChunkMesh {
        Arr<SharingPtr<map::Chunk::Data>, 4> data;
        ChkPos pos;

        BuildChunkMesh() = default;
        BuildChunkMesh(BuildChunkMesh&& that) = default;
        BuildChunkMesh(BuildChunkMesh const& that) = delete;
        BuildChunkMesh& operator=(BuildChunkMesh&& that) = default;
        BuildChunkMesh& operator=(BuildChunkMesh const& that) = delete;
    };
    struct UnloadChunk {
        ChkPos pos;
        OwningPtr<map::Chunk::Data> data;
    };

    union {
        Uninit<GenerateHeightMap> generate_height_map;
        Uninit<LoadChunk>         load_chunk;
        Uninit<UnloadChunk>       unload_chunk;
        Uninit<BuildChunkMesh>    build_chunk_mesh;
    };

    template<typename T>
    TaskInput(T&& in);
    ~TaskInput();
    TaskInput(TaskInput&& that);
    TaskInput(TaskInput const& that) = delete;
    TaskInput& operator=(TaskInput&& that);
    TaskInput& operator=(TaskInput const& that) = delete;
};

struct TaskOutput {
    enum Type : U8 {
        DISCARD,
        GENERATE_HEIGHT_MAP,
        LOAD_CHUNK,
        BUILD_CHUNK_MESH,
    } type;

    struct GenerateHeightMap {
        OwningPtr<map::HeightMap> h_map;
    };
    struct LoadChunk {
        struct BlockChange {
            ChkIdx     idx;
            map::Block block;
        };
        OwningPtr<map::Chunk::Data>              data;
        VecMap<ChkPos, DynArr<BlockChange>> block_changes;
    };
    struct BuildChunkMesh {
        DynArr<BlockFace> faces;
    };
    struct Discard {};

    union {
        Uninit<Discard>           discard;
        Uninit<GenerateHeightMap> generate_height_map;
        Uninit<LoadChunk>         load_chunk;
        Uninit<BuildChunkMesh>    build_chunk_mesh;
    };

    TaskOutput(Type const& type);
    ~TaskOutput();
    TaskOutput(TaskOutput&& that);
    TaskOutput(TaskOutput const& that) = delete;
    TaskOutput& operator=(TaskOutput&& that);
    TaskOutput& operator=(TaskOutput const& that) = delete;
};

void do_task(Uninit<TaskOutput>& out, TaskInput& in);

}
