#pragma once

#include <cstdint>
//
#include <lux_shared/common.hpp>
#include <lux_shared/map.hpp>
//
#include <tick_time_data.hpp>

namespace server {

using ClientId = std::uintptr_t;

bool is_client_connected(ClientId id);
void init(U16 server_port, F64 tick_rate);
void deinit();
void tick(TickTimeData const& time_data);

void kick_client(ClientId id, Str reason);

void broadcast(Str str);
bool get_is_running();
void stop();
ClientId get_client_id(Str name);
void make_admin(ClientId id);
void send_mesh_updates(VecSet<ChkPos> const& meshes);

}
