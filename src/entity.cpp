#include <cmath>
#include <cstring>
//
#include <glm/gtx/quaternion.hpp>
#include <bullet/BulletCollision/CollisionDispatch/btCollisionWorld.h>
//
#include <lux_shared/common.hpp>
//
#include <db.hpp>
#include <map.hpp>
#include <entity.hpp>
#include <server.hpp>
#include <tick_time_data.hpp>

namespace entity {

static constexpr ComponentId no_component = static_max_val<ComponentId>();

#define COMPONENTS(m) \
    m(Name, name); \
    m(Food,  food) \
    m(Stack, stack) \
    m(Body, body); \
    m(Terrestial, terrestial) \
    m(Health, health) \
    m(Inventory, inventory) \
    m(Voxel, voxel) \
    m(MustEat, must_eat)

template<typename T>
using ComponentArr = SparseDynArr<T, ComponentId>;

using Name = Arr<char, 32>;
struct Food {
    F32 nutrition;
    Food(F32 _nutrition) : nutrition(_nutrition) { }
};
struct Stack {
    F32 quantity;
    Stack(F32 _quantity) : quantity(_quantity) { }
};
struct Body {
    OwningPtr<btRigidBody> bt_body;

    Body() = delete;
    Body(Body const&) = delete;
    Body(Body&& that) {
        bt_body = that.bt_body;
        that.bt_body = nullptr;
    }
    Body& operator=(Body const&) = delete;
    Body& operator=(Body&&) = delete;
    Body(Vec3F const& pos) {
        bt_body = physics::create_body({pos.x, pos.y, pos.z});
    }
    ~Body() {
        if(bt_body != nullptr) {
            physics::remove_body(bt_body);
        }
    }
};
struct Terrestial {
    static constexpr U16 jump_timer_delay = 64;
    ComponentId body_id;
    U16         jump_timer;
    F32         move_speed;
    F32         jump_force;
    bool        is_on_ground;

    Terrestial(ComponentId _body_id, F32 _move_speed, F32 _jump_force) {
        body_id = _body_id;
        jump_timer = 0;
        move_speed = _move_speed;
        jump_force = _jump_force;
        is_on_ground = false;
    }
};
struct Health {
    struct Stat {
        F32     start_val;
        F32     val;
        F32     change;
        StrBuff name;
        Stat(F32 _start_val, Str _name) : start_val(_start_val), name(_name) {
            val = start_val;
            change = 0.f;
        }
    };
    using StatId = U16;
    SparseDynArr<Stat> stats;
    struct Synapse {
        enum {
            ADD,
            MUL
        } type;
        StatId in_id;
        StatId out_id;

        F32 in_start;
        F32 in_end;
        F32 out_start;
        F32 out_end;

        F32 last_change = 0.f;

        //if returs false, then it has to be erased
        bool eval(SparseDynArr<Stat>& stats) {
            F32 xs = in_start;
            F32 xe = in_end;
            F32 ys = out_start;
            F32 ye = out_end;

            if(not stats.contains(in_id) || not stats.contains(out_id)) {
                return false;
            }
            F32 x  = stats[in_id].start_val;
            F32 y;
            if(xe == xs) {
                y = x >= xs ? ye : ys;
            } else {
                F32 yg, yl;
                if(ys < ye) {
                    yg = ye;
                    yl = ys;
                } else {
                    yg = ys;
                    yl = ye;
                }
                y = clamp(((ye - ys) / (xe - xs)) * (x - xs) + ys, yl, yg);
            }
            last_change = y;
            auto& out = stats[out_id];
                 if(type == ADD) out.change += y;
            else if(type == MUL) out.change += out.start_val * (y - 1.f);
            return true;
        }
    };
    DynArr<Synapse> synapses;
};
struct Voxel {
    BlockId mat_id;
    Voxel(BlockId _mat_id) : mat_id(_mat_id) { }
};
struct Inventory {
    DynArr<EntityId> items;
    ~Inventory() {
        for(auto const& item : items) {
            erase(item);
        }
    }
};
struct MustEat {
    ComponentId health_id;
    Health::StatId hunger_stat_id;
    MustEat(ComponentId _health_id, Health::StatId _hunger_stat_id) :
        health_id(_health_id),
        hunger_stat_id(_hunger_stat_id) { }
};

namespace {
#define COMPONENT_ARR(t, n) ComponentArr<t> n##_arr;
    COMPONENTS(COMPONENT_ARR);
#undef COMPONENT_ARR
}

//entities and components are stored using SparseDynArr(s), removed elements
//will free up ID slots, without shifting the others, so that every element has
//a constant ID. after an element has been removed, some other element can take
//its' place, but only after one tick has passed, this way elements that depend
//on others can see something has been removed and react accordingly

template<typename T, ComponentArr<T>& arr>
struct ComponentHandle {
    ComponentId id;
    bool is_valid() const {
        return id != no_component;
    }
    T& operator*() {
        return arr[id];
    }
    T* operator->() {
        return &arr[id];
    }
    template<typename ...Args>
    void create(Args&& ...args) {
        id = arr.emplace(args...);
    }
    void erase() {
        arr.erase(id);
    }
};

#define COMPONENT_HANDLE_DEF(t, n) using t##Handle = ComponentHandle<t, n##_arr>;
    COMPONENTS(COMPONENT_HANDLE_DEF);
#undef COMPONENT_HANDLE_DEF

struct Entity {
    EntityId id;
#define COMPONENT_HANDLE(t, n) t##Handle n = {no_component};
    COMPONENTS(COMPONENT_HANDLE);
#undef COMPONENT_HANDLE
};

static SparseDynArr<Entity> entities;

EntityId create_entity() {
    ComponentHandle<Health, health_arr> bob;
    EntityId id = entities.emplace();
    entities[id].id = id;
    LUX_LOG("created entity #%u", id);
    return id;
}

EntityId create_player() {
    LUX_LOG("creating new player");
    EntityId id = create_entity();
    auto& player = entities[id];
    player.body.create(Vec3F(40.f, -110.f, 400.f));
    player.terrestial.create(player.body.id, 10.f, 800.f);
    player.health.create();
    Health::StatId life_id   = player.health->stats.emplace(1.f, "Life"_l);
    Health::StatId hunger_id = player.health->stats.emplace(0.f, "Hunger"_l);
    player.health->synapses.emplace(Health::Synapse{Health::Synapse::MUL,
        hunger_id, life_id, 0.4f, 1.f, 1.f, 0.f});
    player.inventory.create();
    player.must_eat.create(player.health.id, hunger_id);
    return id;
}

static bool is_on_ground(Vec3F const& pos, Body& body) {
    F32 constexpr margin = 0.9f;

    Vec3F from(pos.x, pos.y, pos.z);
    Vec3F to(pos.x, pos.y, pos.z - margin);
    btVector3 bt_from = {from.x, from.y, from.z};
    btVector3 bt_to   = {to.x, to.y, to.z};
    btCollisionWorld::AllHitsRayResultCallback results(bt_from, bt_to);
    physics::ray_test(results, from, to);

    for(Uns i = 0; i < (Uns)results.m_hitFractions.size(); i++)
    {
        if(results.m_collisionObjects[i] != body.bt_body) {
            return true;
        }
    }
    return false;
}

void tick(TickTimeData const& ttd) {
    for(auto pair : body_arr) {
        btVector3 bt_min, bt_max;
        pair.v.bt_body->getAabb(bt_min, bt_max);
        Vec3F min(bt_min.x(), bt_min.y(), bt_min.z());
        Vec3F max(bt_max.x(), bt_max.y(), bt_max.z());
        map::guarantee_physics_mesh_for_aabb(floor(min), ceil(max));
        //@TODO we might actually need to do a chunk raycast
    }
    for(auto pair : terrestial_arr) {
        TerrestialHandle terrestial = {pair.k};
        BodyHandle body = {terrestial->body_id};
        if(not terrestial.is_valid() || not body.is_valid()) {
            terrestial.erase();
            continue;
        }
        auto bt_pos = body->bt_body->getCenterOfMassPosition();
        //@TODO a lot of magic numbers here
        Vec3F pos = Vec3F(bt_pos.x(), bt_pos.y(), bt_pos.z() - 1.f);
        terrestial->is_on_ground =
            is_on_ground(pos + Vec3F(-0.8f,  0.4f, 0.f), *body) |
            is_on_ground(pos + Vec3F( 0.8f,  0.4f, 0.f), *body) |
            is_on_ground(pos + Vec3F(-0.8f, -0.4f, 0.f), *body) |
            is_on_ground(pos + Vec3F( 0.8f, -0.4f, 0.f), *body) |
            is_on_ground(pos + Vec3F( 0.f ,  0.f , 0.f), *body);
        if(terrestial->jump_timer > 0) terrestial->jump_timer--;
        if(terrestial->is_on_ground) {
            body->bt_body->setDamping(0.999f, 0.999f);
        } else {
            body->bt_body->setDamping(0.1f, 0.1f);
        }
    }
    for(auto pair : health_arr) {
        auto& health = pair.v;
        for(Uns i = 0; i < health.synapses.len;) {
            if(not health.synapses[i].eval(health.stats)) {
                health.synapses.erase(i);
            } else ++i;
        }
        for(auto stat : health.stats) {
            stat.v.val = stat.v.start_val + stat.v.change;
            stat.v.change = 0.f;
        }
    }
    //@TODO do only every nth tick
    for(auto pair : must_eat_arr) {
        auto& me = pair.v;
        HealthHandle health = {me.health_id};
        if(not health.is_valid()) {
            MustEatHandle{pair.k}.erase();
            continue;
        }
        F32& hunger = health->stats[me.hunger_stat_id].start_val;
        hunger = clamp(hunger + 0.0001f, 0.f, 1.f);
    }
#define FREE_SLOTS(t, n) n##_arr.free_slots();
    COMPONENTS(FREE_SLOTS);
#undef FREE_SLOTS
    entities.free_slots();
}

void erase(EntityId id) {
    LUX_ASSERT(entities.contains(id));
    Entity& entity = entities[id];
#define COMPONENT_ERASE(t, n) \
        if(entity.n.is_valid()) entity.n.erase();
    COMPONENTS(COMPONENT_ERASE);
#undef COMPONENT_ERASE
    LUX_LOG("removing entity %u", id);
    entities.erase(id);
}

void deinit() {
    for(auto const& p : entities) erase(p.k);
}

void get_net_entities(NetSsTick::Entities& out) {
    for(auto const& entity : entities) {
        if(entity.v.body.is_valid()) {
            auto bt_pos =
                entity.v.body->bt_body->getCenterOfMassPosition();
            Vec3F pos = Vec3F(bt_pos.x(), bt_pos.y(), bt_pos.z());
            out.pos[entity.k] = pos;
        }
        if(entity.v.name.is_valid()) {
            memcpy(out.name[entity.k], *entity.v.name,
                   sizeof(out.name[entity.k]));
        }
        if(entity.v.health.is_valid()) {
            auto& stats_out = out.health[entity.k].stats;
            stats_out.reserve_exactly(entity.v.health->stats.size());
            for(auto pair : entity.v.health->stats) {
                stats_out.push({pair.v.name, pair.v.val, pair.v.change});
            }
        }
        if(entity.v.inventory.is_valid()) {
            auto const& inv = *entity.v.inventory;
            auto& inv_out   = out.inventory[entity.k];
            inv_out.items.reserve_exactly(inv.items.len);
            for(auto const& item : inv.items) {
                inv_out.items.emplace(item);
            }
        }
        if(entity.v.voxel.is_valid()) {
            out.voxel_item[entity.k] = entity.v.voxel->mat_id;
        }
        if(entity.v.stack.is_valid()) {
            out.stack[entity.k] = entity.v.stack->quantity;
        }
    }
}

template<>
bool try_get_controller(Uninit<PlayerController>& out, EntityId id) {
    if(not entities.contains(id)) return false;
    auto& entity = entities[id];
    if(not entity.body.is_valid() ||
       not entity.inventory.is_valid() ||
       not entity.terrestial.is_valid()) return false;
    out.init(); //@TODO use ctor
    out->body_id       = entity.body.id;
    out->terrestial_id = entity.terrestial.id;
    out->inventory_id  = entity.inventory.id;
    out->must_eat_id   = entity.must_eat.id;
    return true;
}

void PlayerController::move(Vec2F const& dir) {
    LUX_ASSERT(length(dir) == 1.f);

    TerrestialHandle terrestial = {terrestial_id};
    if(terrestial->is_on_ground) {
        BodyHandle body = {body_id};
        auto const& bt_quat = body->bt_body->getOrientation();
        glm::quat quat(bt_quat.w(), bt_quat.x(), bt_quat.y(), bt_quat.z());
        Vec2F vel = Vec2F(glm::rotate(quat, Vec3F(dir, 0.f)));
        vel = normalize(vel);
        vel *= terrestial->move_speed;
        auto current = body->bt_body->getLinearVelocity();
        body->bt_body->setLinearVelocity({vel.x, vel.y, current.z()});
    }
}

void PlayerController::jump() {
    TerrestialHandle terrestial = {terrestial_id};
    if(terrestial->is_on_ground && terrestial->jump_timer == 0) {
        BodyHandle body = {body_id};
        body->bt_body->applyCentralImpulse({0.f, 0.f, terrestial->jump_force});
        terrestial->jump_timer = Terrestial::jump_timer_delay;
    }
}

void PlayerController::set_yaw_pitch(F32 yaw, F32 pitch) {
    BodyHandle body = {body_id};
    btTransform tr = body->bt_body->getCenterOfMassTransform();
    glm::quat quat(1.f, 0.f, 0.f, 0.f);
    quat = glm::angleAxis(yaw  , Vec3F(0.f, 0.f, 1.f));
           //glm::angleAxis(pitch, Vec3F(0.f, 1.f, 0.f));
    btQuaternion bt_quat = {quat.x, quat.y, quat.z, quat.w};
    tr.setRotation(bt_quat);

    body->bt_body->setCenterOfMassTransform(tr);
}

void PlayerController::break_block() {
    BodyHandle body = {body_id};
    auto bt_pos = body->bt_body->getCenterOfMassPosition();
    EntityVec pos = EntityVec(bt_pos.x(), bt_pos.y(), bt_pos.z());
    btQuaternion bt_quat = body->bt_body->getOrientation();
    glm::quat quat = {bt_quat.w(), bt_quat.x(), bt_quat.y(), bt_quat.z()};
    EntityVec dst = glm::rotate(quat, EntityVec(32.f, 0.f, 0.f)) + pos;
    EntityVec dummy;
    MapPos hit_pos;
    if(map::cast_ray(&hit_pos, &dummy, pos, dst)) {
        map::Block block = map::read_block(hit_pos);
        auto& items = InventoryHandle{inventory_id}->items;
        bool needs_new = true;
        for(auto& item : items) {
            Entity& entity = entities[item];
            if(entity.voxel.is_valid() && entity.voxel->mat_id == block.id) {
                if(not entity.stack.is_valid()) {
                    entity.stack.create(1.f);
                }
                entity.stack->quantity += 1.f;
                needs_new = false;
            }
        }
        if(needs_new) {
            EntityId new_item = create_entity();
            entities[new_item].voxel.create(block.id);
            items.emplace(new_item);
        }
        map::write_block(hit_pos, {void_block});
    }
}

void PlayerController::place_block() {
    BodyHandle body = {body_id};
    auto const& bt_pos = body->bt_body->getCenterOfMassPosition();
    EntityVec pos = EntityVec(bt_pos.x(), bt_pos.y(), bt_pos.z());
    btQuaternion bt_quat = body->bt_body->getOrientation();
    glm::quat quat = {bt_quat.w(), bt_quat.x(), bt_quat.y(), bt_quat.z()};
    EntityVec dst = glm::rotate(quat, EntityVec(32.f, 0.f, 0.f)) + pos;
    EntityVec normal;
    MapPos hit_pos;
    if(map::cast_ray(&hit_pos, &normal, pos, dst)) {
        map::write_block(hit_pos - (MapPos)normal, {1}); //@TODO id?
    }
}

void PlayerController::eat(EntityId id) {
    if(not entities.contains(id)) return; //TODO error
    InventoryHandle inv = {inventory_id};
    Uns inv_id = -1;
    for(Uns i = 0; i < inv->items.len; ++i) {
        auto const& item = inv->items[i];
        if(item == id) {
            inv_id = i;
            break;
        }
    }
    if(inv_id == -1) return; //@TODO error

    MustEatHandle eater = {must_eat_id};
    //@TODO check for health here, or assign it inside controller
    HealthHandle health = {eater->health_id};
    F32& hunger = health->stats[eater->hunger_stat_id].start_val;
    hunger = clamp(hunger - 0.3f, 0.f, 1.f);

    Entity& entity = entities[id]; //@TODO entityHandle
    bool needs_erase = false;
    if(entity.stack.is_valid() && entity.stack->quantity > 1.f) {
        entity.stack->quantity -= 1.f;
    } else {
        inv->items.erase(inv_id);
        erase(id);
    }
}

}
