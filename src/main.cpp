#include <atomic>
#include <thread>
#include <mutex>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
//
#include <lux_shared/common.hpp>
#include <lux_shared/util/tick_clock.hpp>
//
#include <db.hpp>
#include <map.hpp>
#include <entity.hpp>
#include <server.hpp>
#include <worker.hpp>
#include <tick_time_data.hpp>

std::atomic<bool> exiting(false);

void console_main() {
    std::string input;
    while(!exiting) {
        std::getline(std::cin, input);
        if(input == "stop") {
            exiting = true;
        }
    }
}

int main(int argc, char** argv) {
    //random_seed = std::time(nullptr);

    U16 server_port = 31337;
    { ///read commandline args
        if(argc == 1) {
            LUX_LOG("no commandline arguments given");
            LUX_LOG("assuming server port %u", server_port);
        } else {
            if(argc != 2) {
                LUX_FATAL("usage: %s SERVER_PORT", argv[0]);
            }
            U64 raw_server_port = std::atol(argv[1]);
            if(raw_server_port >= 1 << 16) {
                LUX_FATAL("invalid port %zu given", raw_server_port);
            }
            server_port = raw_server_port;
        }
    }

    worker::init();
    LUX_DEFER { worker::deinit(); };
    db_init();
    constexpr U16 TICK_RATE = 64;
    physics::init();
    LUX_DEFER { physics::deinit(); };
    LUX_DEFER {entity::deinit();};
    server::init(server_port, TICK_RATE);
    LUX_DEFER { server::deinit(); };
    LUX_DEFER { map::deinit(); };
    entity::create_player();

    std::thread console_thread = std::thread(&console_main);
    { ///main loop
        auto tick_len = util::TickClock::Duration(1.0 / TICK_RATE);
        util::TickClock clock(tick_len);
        Uns constexpr ticks_per_day = TICK_RATE * 60 * 24;
        TickTimeData time_data;
        time_data.tick_num = 0;
        time_data.day_cycle = 0.f;
        time_data.tick_rate = TICK_RATE;
        time_data.delta = 1.0 / TICK_RATE;
        auto before = std::chrono::steady_clock::now();
        while(not exiting && server::get_is_running()) {
            clock.start();
            time_data.day_cycle = 0.25f + 0.f;
                //(F32)(time_data.tick_num % ticks_per_day) / (F32)ticks_per_day;
            auto after = std::chrono::steady_clock::now();
            std::chrono::duration<F64> dur = after - before;
            time_data.delta = dur.count();
            before = after;
            benchmark("entities tick", 1.0 / TICK_RATE, [&](){
                entity::tick(time_data);});
            benchmark("map tick"     , 1.0 / TICK_RATE, [&](){
                map::tick(time_data);});
            //@TODO maybe we should handle input on the beginning of each tick,
            //and output and the end of it
            benchmark("server tick"  , 1.0 / TICK_RATE, [&](){
                server::tick(time_data);});

            time_data.tick_num++;
            clock.stop();
            auto remaining = clock.synchronize();
            if(remaining < util::TickClock::Duration::zero()) {
                LUX_LOG("tick overhead of %.2fs", std::abs(remaining.count()));
            }
        }
        if(server::get_is_running()) server::stop();
    }
    console_thread.join();
    return 0;
}
