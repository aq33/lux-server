#pragma once

#include <shared_mutex>
//
#include <lux_shared/map.hpp>
//
#include <db.hpp>
#include <tick_time_data.hpp>

namespace map {

struct Block {
    BlockId id;
};

struct ChunkPhysicsMesh;

using HeightMap = Arr<F32, (CHK_SIZE + 2) * (CHK_SIZE + 2)>;
using HeightMapPos = Vec2<ChkCoord>;

//every mesh stores for the respective chunk's voxels, their "positive" faces,
//i.e. the ones that are +1 XYZ, we still store the orientation for every face,
//which represents the normal's sign as 1 bit, and then the axis (XYZ) as 2 bits
struct ChunkMesh {
    DynArr<BlockFace> faces;
    //@TODO move these to a different container
    DynArr<Uns>       removed_faces;
    DynArr<BlockFace> added_faces;

    OwningPtr<ChunkPhysicsMesh> physics_mesh;

    ChunkMesh() = default;
    ~ChunkMesh();

    enum State : U8 {
        BUILT_EMPTY,
        BUILT_TRIANGLE,
        BUILT_PHYSICS
    } state;
};

struct Chunk {
    struct Data {
        Arr<Block, CHK_VOL> blocks;
        std::shared_mutex mutex;
        //switch to false if you want chunks that have not been changed
        //not to be saved to disk
        bool has_been_changed = true;

        void write(ChkIdx idx, Block const& val);
        Block read(ChkIdx idx);
    };
    //the data is a pointer so that we can freely pass it, without having to
    //worry about iterator invalidation (but we still have to care for chunk
    //unloading), it also probably reduces memory usage significantly due to how
    //unordered_map works, however it might also reduce cache friendliness
    //@TODO make sure we don't delete the chunk if someone holds the data ptr
    OwningPtr<Data> data;
    IdSet<ChkIdx> updated_blocks;

    //@NOTE data can be accessed directly during chunk's generation
    //@NOTE those are for data access only, i.e. they don't mark the chunk as
    //updated, among other things
    void write(ChkIdx idx, Block const& val);
    Block read(ChkIdx idx);
};

Block read_block(MapPos const& pos);
BlockBp const& get_block_bp(MapPos const& pos);

void tick(TickTimeData const&);
void guarantee_chunk(ChkPos const& pos);
bool try_guarantee_chunk(ChkPos const& pos);
bool try_guarantee_chunk_mesh(ChkPos const& pos);

//this function checks if the given chunks are loaded, if not, it enqueues them
//to the loader
void enqueue_missing_chunks_meshes(VecSet<ChkPos> const& requests);
void guarantee_physics_mesh_for_aabb(MapPos const& min, MapPos const& max);
ChunkMesh const& get_chunk_mesh(ChkPos const& pos);
void write_block(MapPos const& pos, Block const& val);

bool cast_ray(MapPos* out_pos, Vec3F* out_dir, Vec3F src, Vec3F dst);

void anchor_chunk(ChkPos const& pos);
void deanchor_chunk(ChkPos const& pos);

void anchor_mesh(ChkPos const& pos);
void deanchor_mesh(ChkPos const& pos);

void deinit();

struct ChunkAnchor {
    bool is_owning;
    ChkPos pos;

    ChunkAnchor(ChkPos);
    ~ChunkAnchor();
    ChunkAnchor(ChunkAnchor&& that);
    ChunkAnchor(ChunkAnchor const& that) = delete;
    ChunkAnchor& operator=(ChunkAnchor&& that);
    ChunkAnchor& operator=(ChunkAnchor const& that) = delete;
};

struct MeshAnchor {
    bool is_owning;
    ChkPos pos;

    MeshAnchor(ChkPos);
    ~MeshAnchor();
    MeshAnchor(MeshAnchor&& that);
    MeshAnchor(MeshAnchor const& that) = delete;
    MeshAnchor& operator=(MeshAnchor&& that);
    MeshAnchor& operator=(MeshAnchor const& that) = delete;
};

}
