#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
//
#include <worker.hpp>
#include <worker_impl.hpp>

namespace worker {

static std::atomic<bool> is_running(false);

struct Worker {
    static constexpr TaskId no_task = -1;
    std::thread thread;
    std::atomic<TaskId> current_task;

    Worker() : current_task(no_task) { }
    //@TODO we won't need that if we ensure that no copy is done, i.e. workers
    //can get allocated only once
    Worker(Worker&& that) :
        thread(move(that.thread)),
        current_task(that.current_task.load()) { }
};

static DynArr<Worker> workers;

struct {
    //@NOTE we assume the queue is sorted, i.e. get_next_task_id always returns
    //greatest ID, and the queue's elements are not rearranged
    Deque<TaskInput>          queue;
    std::condition_variable   cv;
    std::mutex                mutex;
} task_inputs;

struct {
    IdMap<TaskId, TaskOutput> map;
    std::condition_variable   cv;
    std::mutex                mutex;
} task_outputs;

Uns task_queue_len() {
    task_inputs.mutex.lock();
    Uns rval = task_inputs.queue.size();
    task_inputs.mutex.unlock();
    return rval;
}

template<typename T>
TaskId enqueue_task(T&& in) {
    task_inputs.mutex.lock();
    task_inputs.queue.emplace_back(move(in));
    TaskId id = task_inputs.queue.back().id;
    task_inputs.mutex.unlock();
    task_inputs.cv.notify_one();
    return id;
}

//@TODO can we move this to impl file?
template
TaskId enqueue_task<TaskInput::GenerateHeightMap>(TaskInput::GenerateHeightMap&&);
template
TaskId enqueue_task<TaskInput::LoadChunk>(TaskInput::LoadChunk&&);
template
TaskId enqueue_task<TaskInput::UnloadChunk>(TaskInput::UnloadChunk&&);
template
TaskId enqueue_task<TaskInput::BuildChunkMesh>(TaskInput::BuildChunkMesh&&);

template<typename T>
TaskId enqueue_tasks(Slice<T&&> const& ins) {
    task_inputs.mutex.lock();
    LUX_ASSERT(ins.len > 0);
    TaskId first_id;
    {   TaskInput& task = task_inputs.queue.emplace_back(move(ins[0]));
        first_id = task.id;
    }
    for(Uns i = 1; i < ins.len; ++i) {
        TaskInput& task = task_inputs.queue.emplace_back(move(ins[i]));
    }
    task_inputs.mutex.unlock();
    task_inputs.cv.notify_all();
    return first_id;
}

static void worker_main(Uns worker_id) {
    while(true) {
        std::unique_lock<std::mutex> lock(task_inputs.mutex);
        task_inputs.cv.wait(lock,
            []{return not task_inputs.queue.empty() || not is_running.load();});
        if(not is_running.load()) {
            break;
        }
        TaskInput in = move(task_inputs.queue.front());
        workers[worker_id].current_task = in.id;
        task_inputs.queue.pop_front();
        lock.unlock();
        Uninit<TaskOutput> out;
        do_task(out, in);
        task_outputs.mutex.lock();
        task_outputs.map.emplace(in.id, move(*out));
        workers[worker_id].current_task = Worker::no_task;
        task_outputs.mutex.unlock();
        out.deinit();
        task_outputs.cv.notify_all();
    }
}

bool try_take_result(TaskOutput& out, TaskId id) {
    if(task_outputs.mutex.try_lock()) {
        LUX_DEFER { task_outputs.mutex.unlock(); };
        if(task_outputs.map.count(id) > 0) {
            out = move(task_outputs.map.at(id));
            task_outputs.map.erase(id);
            return true;
        }
    }
    return false;
}

template<typename It>
static It search_task(It beg, It end, TaskId id) {
    auto len = end - beg;
    LUX_ASSERT(len > 0);
    auto half = beg + (len - 1) / 2;
    if(half->id == id) {
        return half;
    } else if(half->id < id) {
        return search_task(half + 1, end, id);
    } else if(half->id > id) {
        return search_task(beg, half, id);
    }
    LUX_UNREACHABLE();
    return end; //to supress the warning
}

void force_take_result(TaskOutput& out, TaskId id) {
    std::unique_lock<std::mutex> lock(task_outputs.mutex);
    if(task_outputs.map.count(id) > 0) {
        out = move(task_outputs.map.at(id));
        task_outputs.map.erase(id);
    } else {
        //so that no worker takes the task after we ensured it isn't doing it
        task_inputs.mutex.lock();
        for(auto const& worker : workers) {
            if(worker.current_task == id) {
                task_inputs.mutex.unlock();
                task_outputs.cv.wait(lock,
                    [&]{return task_outputs.map.count(id) > 0;});
                out = move(task_outputs.map.at(id));
                task_outputs.map.erase(id);
                return;
            }
        }
        //nothing so far, so the task has not began processing yet
        //assuming no shenanigans, the input queue will be naturally sorted by
        //task's id, we can do binary search
        auto it = search_task(task_inputs.queue.begin(),
                              task_inputs.queue.end(), id);
        TaskInput in(move(*it));
        task_inputs.queue.erase(it);
        task_inputs.mutex.unlock();
        lock.unlock();
        Uninit<TaskOutput> temp_out;
        do_task(temp_out, in);
        out = move(*temp_out);
    }
}

void init() {
    is_running = true;
    Uns thread_num = std::thread::hardware_concurrency();
    LUX_LOG("worker thread number: %zu", thread_num);
    for(Uns i = 0; i < thread_num; i++) {
        workers.emplace();
    }
    for(Uns i = 0; i < workers.len; i++) {
        workers[i].current_task = Worker::no_task;
        workers[i].thread = std::thread(&worker_main, i);
    }
}

void deinit() {
    is_running = false;
    task_inputs.cv.notify_all();
    for(auto& worker : workers) {
        worker.thread.join();
    }
    workers.clear();
}

}
